// Netify Agent
// Copyright (C) 2015-2019 eGloo Incorporated <http://www.egloo.ca>
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include <stdexcept>
#include <vector>
#include <map>
#include <unordered_map>
#include <string>
#include <sstream>
#ifdef HAVE_ATOMIC
#include <atomic>
#else
typedef bool atomic_bool;
#endif
#include <regex>

#include <unistd.h>
#include <stdlib.h>
#include <signal.h>
#include <pthread.h>
#include <dlfcn.h>
#include <string.h>
#include <errno.h>

#include <sys/types.h>
#include <sys/stat.h>
#include <sys/socket.h>

#include <json.h>
#include <pcap/pcap.h>

using namespace std;

class ndPluginLoader;

#include <netifyd/netifyd.h>
#include <netifyd/nd-util.h>
#include <netifyd/nd-json.h>
#include <netifyd/nd-thread.h>
#include <netifyd/nd-plugin.h>

#include "plugin-example.h"

ndPluginExampleService::ndPluginExampleService(const string &tag)
    : ndPluginService(tag)
{
    nd_debug_printf("%s: initialized\n", tag.c_str());
}

ndPluginExampleService::~ndPluginExampleService()
{
    Join();

    nd_debug_printf("%s: destroyed\n", tag.c_str());
}

void *ndPluginExampleService::Entry(void)
{
    string uuid_dispatch;
    ndJsonPluginParams params;

    nd_debug_printf("%s: Hello, Plugin Service!\n", tag.c_str());

    while (! ShouldTerminate()) {

        while (PopParams(uuid_dispatch, params)) {

            for (ndJsonPluginParams::const_iterator i = params.begin();
                i != params.end(); i++) {

                PushReplyLock(uuid_dispatch, i->first, i->second);
            }
        }

        sleep(1);
    }

    return NULL;
}

ndPluginInit(ndPluginExampleService);

// vi: expandtab shiftwidth=4 softtabstop=4 tabstop=4
