// Netify Agent
// Copyright (C) 2015-2019 eGloo Incorporated <http://www.egloo.ca>
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include <stdexcept>
#include <vector>
#include <map>
#include <unordered_map>
#ifdef HAVE_ATOMIC
#include <atomic>
#else
typedef bool atomic_bool;
#endif
#include <sstream>
#include <regex>

#include <unistd.h>
#include <stdlib.h>
#include <signal.h>
#include <pthread.h>
#include <dlfcn.h>
#include <string.h>
#include <errno.h>

#include <sys/types.h>
#include <sys/stat.h>
#include <sys/socket.h>

#include <json.h>
#include <pcap/pcap.h>

using namespace std;

class ndPluginLoader;

#include <netifyd/netifyd.h>
#include <netifyd/nd-util.h>
#include <netifyd/nd-json.h>
#include <netifyd/nd-thread.h>
#include <netifyd/nd-plugin.h>

#include "plugin-example.h"

ndPluginExampleTask::ndPluginExampleTask(const string &tag)
    : ndPluginTask(tag)
{
    nd_debug_printf("%s: initialized\n", tag.c_str());
}

ndPluginExampleTask::~ndPluginExampleTask()
{
    Join();

    nd_debug_printf("%s: destroyed\n", tag.c_str());
}

void *ndPluginExampleTask::Entry(void)
{
    ndJsonPluginParams params;

    nd_debug_printf("%s: Hello, Plugin Task!\n", tag.c_str());

    PushReply(tag.c_str(), "Hello, Plugin Task!\n");

    while (PopParams(params)) {

        for (ndJsonPluginParams::const_iterator i = params.begin();
            i != params.end(); i++) {

            PushReplyLock(i->first, i->second);
        }
    }

    sleep(3);

    return NULL;
}

ndPluginInit(ndPluginExampleTask);

// vi: expandtab shiftwidth=4 softtabstop=4 tabstop=4
