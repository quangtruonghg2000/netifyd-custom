// Netify Agent
// Copyright (C) 2015-2019 eGloo Incorporated <http://www.egloo.ca>
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

#ifndef _ND_PLUGIN_EXAMPLE_H
#define _ND_PLUGIN_EXAMPLE_H

class ndPluginExampleService : public ndPluginService
{
public:
    ndPluginExampleService(const string &tag);
    virtual ~ndPluginExampleService();

    virtual void *Entry(void);
};

class ndPluginExampleTask : public ndPluginTask
{
public:
    ndPluginExampleTask(const string &tag);
    virtual ~ndPluginExampleTask();

    virtual void *Entry(void);
};

#endif // _ND_PLUGIN_EXAMPLE_H
// vi: expandtab shiftwidth=4 softtabstop=4 tabstop=4
