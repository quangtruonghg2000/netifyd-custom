#include <stdexcept>
#include <unordered_map>
#include <list>
#include <vector>
#include <sstream>
#include <atomic>
#include <regex>
#include <mutex>
#include <bitset>

#include <sys/stat.h>
#include <sys/select.h>
#include <sys/socket.h>
#include <sys/types.h>

#include <unistd.h>
#include <errno.h>
#include <fcntl.h>
#include <pthread.h>
#include <signal.h>
#include <string.h>
#include <time.h>
#include <arpa/inet.h>

#include <linux/netlink.h>
#include <linux/rtnetlink.h>
#include <net/if.h>
#include <net/if_arp.h>
#include <arpa/inet.h>

#define __FAVOR_BSD 1
#include <netinet/tcp.h>
#undef __FAVOR_BSD

#include <net/if.h>
#include <net/if_arp.h>
#include <linux/if_ether.h>
#include <linux/if_packet.h>

#include <pcap/pcap.h>

#include <libmnl/libmnl.h>
#include <libnetfilter_conntrack/libnetfilter_conntrack.h>

#include <nlohmann/json.hpp>
using json = nlohmann::json;

using namespace std;
#include "netifyd.h"

#include "nd-ndpi.h"
#include "nd-netlink.h"
#include "nd-json.h"
#include "nd-flow.h"
#include "nd-thread.h"
#include "nd-util.h"
#include "nd-neigh.h"

#define NETLINK_TEST 1
#define DEBUG 0

ndNeighThread *neigh_class;
list_ip *list_ip_head = NULL;	
unordered_map<string, info>ump;


void list_ip_insert_tail(list_ip *node) {
	list_ip *tmp = list_ip_head;

	if (!tmp) {
		list_ip_head = node;
	} else {
		while(tmp->next) {
			tmp = tmp->next;
		}

		tmp->next = node;
	}
}

void list_ip_insert(char *ip) {
	list_ip *node = (list_ip *)malloc(sizeof(list_ip));
	strncpy(ip,node->ip,16);
	node->next = NULL;
	list_ip_insert_tail(node);
}

void list_ip_del(char *ip) {
	list_ip *tmp = list_ip_head;
	list_ip *tmp_prev;

	if (!strcmp(ip, tmp->ip)) {
		list_ip_head = tmp->next;
		free(tmp);
		return;
	}
	else {
		while(tmp) {
			if (!strcmp(ip, tmp->ip)) {
				tmp_prev->next = tmp->next;
				free(tmp);
				return;
			}
			tmp_prev = tmp;
			tmp = tmp->next;
		}
	}
}

list_ip *ndNeighThread::list_ip_find(char *ip) {
	list_ip *tmp = list_ip_head;

	while(tmp) {
		if (!strcmp(ip, tmp->ip)) return tmp;
		tmp = tmp->next;
	}

	return NULL;
}

void list_ip_show() {
	printf("list_ip: ");
	list_ip *tmp = list_ip_head;

	while(tmp) {
		printf("%s, ", tmp->ip);
		tmp = tmp->next;
	}

	printf("\n");
}

char *ll_index_to_name(unsigned int idx)
{
	static char buf[IFNAMSIZ];

	if (if_indextoname(idx, buf) == NULL)
		snprintf(buf, IFNAMSIZ, "if%u", idx);

	return buf;
}

#if NETLINK_TEST
#ifndef NDA_RTA
#define NDA_RTA(r) \
	((struct rtattr *)(((char *)(r)) + NLMSG_ALIGN(sizeof(struct ndmsg))))
#endif

const char *rt_addr_n2a_r(int af, int len,
			  const void *addr, char *buf, int buflen)
{
	switch (af) {
	case AF_INET:
	case AF_INET6:
		return inet_ntop(af, addr, buf, buflen);
	default:
		return "???";
	}
}

const char *format_host_r(int af, int len, const void *addr,
			char *buf, int buflen)
{

	return rt_addr_n2a_r(af, len, addr, buf, buflen);
}

const char *format_host(int af, int len, const void *addr)
{
	static char buf[256];

	return format_host_r(af, len, addr, buf, 256);
}

#define format_host_rta(af, rta) \
	format_host(af, RTA_PAYLOAD(rta), RTA_DATA(rta))

typedef uint8_t switchlink_mac_addr_t[6];

int parse_rtattr_flags(struct rtattr *tb[], int max, struct rtattr *rta,
		       int len, unsigned short flags)
{
	unsigned short type;

	memset(tb, 0, sizeof(struct rtattr *) * (max + 1));
	while (RTA_OK(rta, len)) {
		type = rta->rta_type & ~flags;
		if ((type <= max) && (!tb[type]))
			tb[type] = rta;
		rta = RTA_NEXT(rta, len);
	}
	if (len)
		fprintf(stderr, "!!!Deficit %d, rta_len=%d\n",
			len, rta->rta_len);
	return 0;
}

int parse_rtattr(struct rtattr *tb[], int max, struct rtattr *rta, int len)
{
	return parse_rtattr_flags(tb, max, rta, len, 0);
}

void convertMac(string mac){
    const char* charArray = mac.c_str();
    unsigned char* uc = new unsigned char[mac.length()];

    for (int i = 0; i < mac.length(); i++) {
        uc[i] = static_cast<unsigned char>(charArray[i]);
    }
	printf("MAC address: %02x:%02x:%02x:%02x:%02x:%02x ",
        mac[0], mac[1], mac[2], mac[3], mac[4], mac[5]);
    delete[] uc;
}

void process_neigh_event(unsigned char* mac, const char *dst, char *dev, int action) {
// 	struct info {
// 		char* dev;
// 		const char* ip;
// 		int add_lock; 
// 	}
// 	unordered_map < string, struct info > ump;
	string key((char*)mac,(char*)mac+6);

	switch(action) 
	{
		case 1:
		{
			printf("prepare to add ip ...\n");
			struct info val1={dev,NULL,0};
			ump[key] = val1;
			break;
		}
		case 2:		
		{	
			const char* dest_ip = (const char*)malloc(16);
			strcpy((char*)dest_ip,(char *)dst);
			if( ump.find(key) != ump.end() && ump.at(key).add_lock == 0){
				printf("Store ip ... ");
				struct info val2={dev,dest_ip,1};
				ump[key] = val2;
				list_ip_insert((char*)dest_ip);     list_ip_show();
				// printf("\n"); convertMac(key);
				// cout << "ip:" <<ump[key].ip << " " << ump[key].add_lock << endl;
			}
			break;
		}
		default:
			if( ump.find(key) != ump.end() && ump[key].ip != NULL ){
				printf("erase ip ...");
				list_ip_del((char*)(ump[key].ip)); list_ip_show();
				// printf("\n"); convertMac(key);
				// cout << "ip:" <<ump[key].ip << " " << ump[key].add_lock << endl;
				ump.erase(key);
			}
			break;
	};
};

void process_neigh_msg(struct nlmsghdr *n, int type) {
    struct ndmsg *r = static_cast<struct ndmsg *>(NLMSG_DATA(n));
	int len = n->nlmsg_len;
	struct rtattr *tb[NDA_MAX+1];
    int ipv4 = 0;
	__u8 protocol = 0;
	const char *dst;
    char *dev = (char *)malloc(16);
    unsigned char *mac;
    int family;
	char iface[16];


	len -= NLMSG_LENGTH(sizeof(*r));
	if (len < 0) {
		fprintf(stderr, "BUG: wrong nlmsg len %d\n", len);
		return;
	}

	parse_rtattr(tb, NDA_MAX, NDA_RTA(r), n->nlmsg_len - NLMSG_LENGTH(sizeof(*r)));
	struct ifaddrmsg *ifa = (struct ifaddrmsg *) NLMSG_DATA(n);
	if_indextoname(ifa->ifa_index, iface);

	if (tb[NDA_DST]) {
		family = r->ndm_family;

		if (family == AF_BRIDGE) {
			if (RTA_PAYLOAD(tb[NDA_DST]) == sizeof(struct in6_addr))
				family = AF_INET6;
			else
				family = AF_INET;
		}
		dst = format_host_rta(family, tb[NDA_DST]);
		printf("->dst %s  ", dst);
	}

    if(tb[NDA_LLADDR]){
        struct rtattr *rta = tb[NDA_LLADDR];
        mac = (unsigned char *)RTA_DATA(rta);
        printf("MAC address: %02x:%02x:%02x:%02x:%02x:%02x ",
        mac[0], mac[1], mac[2], mac[3], mac[4], mac[5]);
    }

	if (r->ndm_ifindex) {
        dev = ll_index_to_name(r->ndm_ifindex);
		printf("->dev %s ", dev);
	}

    printf("State: %d ",r->ndm_state);
	
	if(type == RTM_NEWNEIGH && r->ndm_state == 2 && !tb[NDA_DST] && (strcmp(iface, "br-lan") != 0)  ) {
		process_neigh_event(mac, NULL, dev, 1);
	}

    else if(type == RTM_NEWNEIGH && r->ndm_state == 2 && family == AF_INET ) {
		process_neigh_event(mac, dst, dev, 2);
    }

    else if(type == RTM_DELNEIGH && !tb[NDA_DST] && r->ndm_state == 2) {
		process_neigh_event(mac, NULL, dev, 0);
    }
};
int process_nl_message(struct nlmsghdr *nlmsg) {
    int store = 0;
    if (nlmsg->nlmsg_type == RTM_NEWNEIGH || nlmsg->nlmsg_type == RTM_DELNEIGH ) {
        process_neigh_msg(nlmsg, nlmsg->nlmsg_type);
    }

    return 0;
}
void *ndNeighThread::Entry() {
   	int status;
	struct nlmsghdr *h;
	struct sockaddr_nl nladdr = { .nl_family = AF_NETLINK };
	struct iovec iov;
	struct msghdr msg = {
		.msg_name = &nladdr,
		.msg_namelen = sizeof(nladdr),
		.msg_iov = &iov,
		.msg_iovlen = 1,
	};
	char   buf[16384];

    iov.iov_base = buf;
	while (true) {
		iov.iov_len = sizeof(buf);
		status = recvmsg(sockfd, &msg, 0);

		if (status < 0) {
			if (errno == EINTR || errno == EAGAIN)
				continue;
			fprintf(stderr, "netlink receive error %s (%d)\n",
				strerror(errno), errno);
			if (errno == ENOBUFS)
				continue;
			return NULL;
		}
		if (status == 0) {
			fprintf(stderr, "EOF on netlink\n");
			return NULL;
		}
		if (msg.msg_namelen != sizeof(nladdr)) {
			fprintf(stderr,
				"Sender address length == %d\n",
				msg.msg_namelen);
			exit(1);
		}

		for (h = (struct nlmsghdr *)buf; status >= sizeof(*h); ) {
			int err;
			int len = h->nlmsg_len;
			int l = len - sizeof(*h);

			if (l < 0 || len > status) {
				if (msg.msg_flags & MSG_TRUNC) {
					fprintf(stderr, "Truncated message\n");
					return NULL;
				}
				fprintf(stderr,
					"!!!malformed message: len=%d\n",
					len);
				exit(1);
			}

			process_nl_message(h);

			status -= NLMSG_ALIGN(len);
			h = (struct nlmsghdr *)((char *)h + NLMSG_ALIGN(len));
		}
		if (msg.msg_flags & MSG_TRUNC) {
			fprintf(stderr, "Message truncated\n");
			continue;
		}
		if (status) {
			fprintf(stderr, "!!!Remnant of size %d\n", status);
			exit(1);
		}
	}
	nd_debug_printf("%s: started\n", __PRETTY_FUNCTION__);
}
#endif

ndNeighThread::ndNeighThread() : ndThread("nd-neigh", 1){
    struct sockaddr_nl addr;
    memset(&addr, 0, sizeof(addr));
    addr.nl_family = AF_NETLINK;
    addr.nl_groups = RTMGRP_NEIGH;  

    sockfd = socket(AF_NETLINK, SOCK_RAW, NETLINK_ROUTE);
    if (sockfd < 0) {
        printf("Failed to create netlink socket.\n");
    };

    if (bind(sockfd, (struct sockaddr *) &addr, sizeof(addr)) < 0) {
        printf("Failed to bind netlink socket.\n");
    };

};