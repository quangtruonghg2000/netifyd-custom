#ifndef _ND_NEIGH_H
#define _ND_NEIGH_H 

struct list_ip {
	struct list_ip *next;
		char ip[16];
};

struct info {
	char* dev;
	const char* ip;
	int add_lock; 
};


class ndNeighThread : public ndThread 
{
	public:
		ndNeighThread();
		list_ip *list_ip_find(char *ip);
		virtual void *Entry(void);
	private:	
		struct sockaddr_nl addr;
		int sockfd;
};

#endif