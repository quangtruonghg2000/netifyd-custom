#!/bin/sh

UPLOAD_WD="/usr/local/var/run/netifyd/upload.wd"

[ ! -f "$UPLOAD_WD" ] && exit 0
 
if [ $[ $(date '+%s') - 30 ] -gt $(stat -c '%Y' "$UPLOAD_WD") ]; then
	/usr/local/etc/init.d/netifyd restart
	#service netifyd restart
	#systemctl netifyd netifyd
fi

exit 0
