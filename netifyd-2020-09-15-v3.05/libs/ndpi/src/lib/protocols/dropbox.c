/*
 * dropbox.c
 *
 * Copyright (C) 2012-18 by ntop.org
 *
 * This file is part of nDPI, an open source deep packet inspection
 * library based on the OpenDPI and PACE technology by ipoque GmbH
 *
 * nDPI is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * nDPI is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with nDPI.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#include "ndpi_protocol_ids.h"

#define NDPI_CURRENT_PROTO NDPI_PROTOCOL_DROPBOX

#include "ndpi_api.h"

#define DB_LSP_PORT     17500

#define DB_HOST_INT     "host_int"
#define DB_HOST_INT_LEN (sizeof(DB_HOST_INT) - 1)

#define DB_BUS17CMD     "Bus17Cmd"
#define DB_BUS17CMD_LEN (sizeof(DB_BUS17CMD) - 1)

static void ndpi_int_dropbox_add_connection(
        struct ndpi_detection_module_struct *ndpi_struct,
        struct ndpi_flow_struct *flow,
        u_int8_t due_to_correlation)
{
    ndpi_set_detected_protocol(ndpi_struct, flow, NDPI_PROTOCOL_DROPBOX);
}

static void ndpi_check_dropbox(
        struct ndpi_detection_module_struct *ndpi_struct,
        struct ndpi_flow_struct *flow)
{
    struct ndpi_packet_struct *packet = &flow->packet;
    u_int32_t payload_len = packet->payload_packet_len;

    if (packet->udp != NULL) {
        if (packet->udp->source == htons(DB_LSP_PORT)) {
            if (payload_len > DB_HOST_INT_LEN) {
                if (ndpi_strnstr((const char *)packet->payload,
                            "\"" DB_HOST_INT "\"", payload_len) != NULL) {

                    NDPI_LOG_INFO(ndpi_struct, "found dropbox (host_int)\n");
                    ndpi_int_dropbox_add_connection(ndpi_struct, flow, 0);
                    return;
                }
            }
        }
        else {
            if (payload_len > DB_BUS17CMD_LEN) {
                if (ndpi_strnstr((const char *)packet->payload,
                            DB_BUS17CMD, payload_len) != NULL) {

                    NDPI_LOG_INFO(ndpi_struct, "found dropbox (Bus17Cmd)\n");
                    ndpi_int_dropbox_add_connection(ndpi_struct, flow, 0);
                    return;
                }
            }
        }
    }

    NDPI_EXCLUDE_PROTO(ndpi_struct, flow);
}

void ndpi_search_dropbox(
        struct ndpi_detection_module_struct *ndpi_struct,
        struct ndpi_flow_struct *flow)
{
    struct ndpi_packet_struct *packet = &flow->packet;

    NDPI_LOG_DBG(ndpi_struct, "search dropbox\n");
}

void init_dropbox_dissector(
        struct ndpi_detection_module_struct *ndpi_struct,
        u_int32_t *id, NDPI_PROTOCOL_BITMASK *detection_bitmask)
{
    ndpi_set_bitmask_protocol_detection("DROPBOX", ndpi_struct, detection_bitmask, *id,
            NDPI_PROTOCOL_DROPBOX,
            ndpi_search_dropbox,
            NDPI_SELECTION_BITMASK_PROTOCOL_UDP_WITH_PAYLOAD,
            SAVE_DETECTION_BITMASK_AS_UNKNOWN,
            ADD_TO_DETECTION_BITMASK);
    *id += 1;
}
