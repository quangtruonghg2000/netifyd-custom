/*
 * tor.c
 *
 * Copyright (C) 2016-18 ntop.org
 * Copyright (C) 2013 Remy Mudingay <mudingay@ill.fr>
 *
 */
#include "ndpi_protocol_ids.h"

#define NDPI_CURRENT_PROTO NDPI_PROTOCOL_TOR

#include "ndpi_api.h"

static void ndpi_int_tor_add_connection(struct ndpi_detection_module_struct
        *ndpi_struct, struct ndpi_flow_struct *flow) {
    ndpi_set_detected_protocol(ndpi_struct, flow, NDPI_PROTOCOL_TOR);
}

/* ******************************************* */

void ndpi_search_tor(struct ndpi_detection_module_struct *ndpi_struct, struct ndpi_flow_struct *flow)
{
    struct ndpi_packet_struct *packet = &flow->packet;
    u_int16_t dport = 0, sport = 0;

    NDPI_LOG_DBG(ndpi_struct, "search for TOR\n");

    if(packet->tcp != NULL) {
        sport = ntohs(packet->tcp->source), dport = ntohs(packet->tcp->dest);
        NDPI_LOG_DBG2(ndpi_struct, "calculating TOR over tcp\n");

        if ((((dport == 9001) || (sport == 9001)) || ((dport == 9030) || (sport == 9030)))
                && ((packet->payload[0] == 0x17) || (packet->payload[0] == 0x16))
                && (packet->payload[1] == 0x03)
                && (packet->payload[2] == 0x01)
                && (packet->payload[3] == 0x00)) {
            NDPI_LOG_INFO(ndpi_struct, "found tor\n");
            ndpi_int_tor_add_connection(ndpi_struct, flow);
        }
    } else {
        NDPI_EXCLUDE_PROTO(ndpi_struct, flow);
    }
}

void init_tor_dissector(struct ndpi_detection_module_struct *ndpi_struct, u_int32_t *id, NDPI_PROTOCOL_BITMASK *detection_bitmask)
{
    ndpi_set_bitmask_protocol_detection("Tor", ndpi_struct, detection_bitmask, *id,
            NDPI_PROTOCOL_TOR,
            ndpi_search_tor,
            NDPI_SELECTION_BITMASK_PROTOCOL_V4_V6_TCP_WITH_PAYLOAD_WITHOUT_RETRANSMISSION,
            SAVE_DETECTION_BITMASK_AS_UNKNOWN,
            ADD_TO_DETECTION_BITMASK);

    *id += 1;
}

