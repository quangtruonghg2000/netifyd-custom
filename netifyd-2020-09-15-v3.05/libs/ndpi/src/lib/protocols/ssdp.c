/*
 * ssdp.c
 *
 * Copyright (C) 2009-2011 by ipoque GmbH
 * Copyright (C) 2011-18 - ntop.org
 *
 * This file is part of nDPI, an open source deep packet inspection
 * library based on the OpenDPI and PACE technology by ipoque GmbH
 *
 * nDPI is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * nDPI is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with nDPI.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#include "ndpi_protocol_ids.h"

#define NDPI_CURRENT_PROTO  NDPI_PROTOCOL_SSDP

#include "ndpi_api.h"

#define SSDP_MSEARCH        "M-SEARCH * HTTP/"
#define SSDP_MSEARCH_LEN    (sizeof(SSDP_MSEARCH) - 1)
#define SSDP_NOTIFY         "NOTIFY * HTTP/"
#define SSDP_NOTIFY_LEN     (sizeof(SSDP_NOTIFY) - 1)

static int ssdp_parse_lines(struct ndpi_detection_module_struct *ndpi_struct,
        struct ndpi_flow_struct *flow) {
    struct ndpi_packet_struct *packet = &flow->packet;

    packet->packet_lines_parsed_complete = 0;
    ndpi_parse_packet_line_info_any(ndpi_struct, flow);
#if 0
    printf("SSDP: parsed_completed: %d, parsed_lines: %d\n",
            packet->packet_lines_parsed_complete,
            packet->parsed_lines
          );

    for (int i = 0; i < packet->parsed_lines; i++) {
        if (packet->line[i].len <= 2) continue;

        char line[256];
        memset(line, 0, 256);
        memcpy(line, packet->line[i].ptr, packet->line[i].len > (256 - 1) ? (256 - 1) : packet->line[i].len);
        printf("SSDP[%d]: %s\n", i, line);
    }
#endif
    return packet->parsed_lines;
}

static void ndpi_int_ssdp_add_connection(struct ndpi_detection_module_struct
        *ndpi_struct, struct ndpi_flow_struct *flow)
{
    struct ndpi_packet_struct *packet = &flow->packet;

    ssdp_parse_lines(ndpi_struct, flow);

    ndpi_set_detected_protocol(ndpi_struct, flow, NDPI_PROTOCOL_SSDP);
}

/* this detection also works asymmetrically */
void ndpi_search_ssdp(struct ndpi_detection_module_struct *ndpi_struct, struct ndpi_flow_struct *flow)
{
    struct ndpi_packet_struct *packet = &flow->packet;

    NDPI_LOG_DBG(ndpi_struct, "search ssdp\n");

    if (packet->udp != NULL) {

        if (packet->payload_packet_len >= SSDP_MSEARCH_LEN &&
                memcmp(packet->payload, SSDP_MSEARCH, SSDP_MSEARCH_LEN) == 0) {
            NDPI_LOG_INFO(ndpi_struct, "found ssdp (M-SEARCH)\n");
            ndpi_int_ssdp_add_connection(ndpi_struct, flow);
            return;
        }

        if (packet->payload_packet_len >= SSDP_NOTIFY_LEN &&
                memcmp(packet->payload, SSDP_NOTIFY, SSDP_NOTIFY_LEN) == 0) {
            NDPI_LOG_INFO(ndpi_struct, "found ssdp (NOTIFY)\n");
            ndpi_int_ssdp_add_connection(ndpi_struct, flow);
            return;
        }
    }

    NDPI_EXCLUDE_PROTO(ndpi_struct, flow);
}

void init_ssdp_dissector(struct ndpi_detection_module_struct *ndpi_struct, u_int32_t *id, NDPI_PROTOCOL_BITMASK *detection_bitmask)
{
    ndpi_set_bitmask_protocol_detection("SSDP", ndpi_struct, detection_bitmask, *id,
            NDPI_PROTOCOL_SSDP,
            ndpi_search_ssdp,
            NDPI_SELECTION_BITMASK_PROTOCOL_V4_V6_UDP_WITH_PAYLOAD,
            SAVE_DETECTION_BITMASK_AS_UNKNOWN,
            ADD_TO_DETECTION_BITMASK);

    *id += 1;
}
